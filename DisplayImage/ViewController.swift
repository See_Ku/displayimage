//
//  ViewController.swift
//  DisplayImage
//
//  Created by See.Ku on 2016/07/02.
//  Copyright (c) 2016 AxeRoad. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	@IBOutlet weak var imageView: UIImageView!

	override func viewDidLoad() {
		super.viewDidLoad()

		// バンドルした画像ファイルを読み込み
		let image = UIImage(named: "fruit_melon_cut_orange")

		// Image Viewに画像を設定
		imageView.image = image
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

}

